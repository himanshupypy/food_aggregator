
# Add these lines at the top of your views.py file
from django.contrib.auth.models import User
from django.conf import settings
from django.contrib.auth import authenticate
from django.shortcuts import render
from rest_framework_jwt.settings import api_settings
from rest_framework import generics, status
from rest_framework import permissions
from rest_framework.response import Response
from django.utils.safestring import mark_safe
import json
from delivery_tasks.helper import delete_key_in_redis, remove_bikers_from_channel_group, add_key_in_redis, \
    get_task_list_by_manager_id
from delivery_tasks.delivery_service import accepted_task_list_of_biker
from .serializer import TokenSerializer
# Get the JWT settings, add these lines after the import/from lines
jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

# ...
FREE_DELIVERY_PERSON_PREFIX = settings.FREE_DELIVERY_PERSON_PREFIX
CURRENT_TASK_KEY = settings.CURRENT_TASK_KEY
TASK_PRIORITY_QUEUE_NAME = settings.TASK_PRIORITY_QUEUE_NAME
REPLACE_TASK_PRIORITY_QUEUE_NAME = settings.REPLACE_TASK_PRIORITY_QUEUE_NAME
TASK_ACCEPTANCE_CHANNEL_GROUP_NAME = settings.TASK_ACCEPTANCE_CHANNEL_GROUP_NAME
# Add this view to your views.py file


def index(request):
    return render(request, 'index.html', {})


def login(request):
    return render(request, 'login.html', {})


def biker_dashboard(request):
    return render(request, 'biker_dashboard.html', {})


def create_task(request):
    return render(request, 'create_task.html', {})


def manager_dashboard(request):
    return render(request, 'manager_dashboard.html',{})


class LoginView(generics.CreateAPIView):

    permission_classes = (permissions.AllowAny,)

    def post(self, request, *args, **kwargs):
        username = request.data.get("username", "")
        password = request.data.get("password", "")
        user_type = request.data.get('user_type', "")
        user = authenticate(request, username=username, password=password)
        if user is not None:
            print('user type ',user.userprofile.user_type)
            if user.userprofile.user_type != user_type:
                return Response({'message': 'user type not matching!'},
                                status=status.HTTP_401_UNAUTHORIZED
                )
            serializer = TokenSerializer(data={
                # using drf jwt utility functions to generate a token
                "token": jwt_encode_handler(
                    jwt_payload_handler(user)
                )
            })
            serializer.is_valid()
            count = accepted_task_list_of_biker(user.userprofile.id)
            if count<3:
                key = f'{FREE_DELIVERY_PERSON_PREFIX}{user.userprofile.id}'
                add_key_in_redis(key, 1)
            return Response({'token': serializer.data['token'],
                             'username': user.username,
                             'user_type': user.userprofile.user_type,
                             'user_id': user.userprofile.id
                             },
                            status=status.HTTP_200_OK)
        return Response({
            'message': 'credential not matching!'
        }, status=status.HTTP_401_UNAUTHORIZED)


class LogoutView(generics.CreateAPIView):

    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        if not request.user:
            return Response({
                'message': 'user not logged in!'
            }, status=status.HTTP_401_UNAUTHORIZED)
        if request.user.userprofile.user_type == 'D':
            # remove biker from free biker list in redis
            key = f'{FREE_DELIVERY_PERSON_PREFIX}{request.user.userprofile.id}'
            delete_key_in_redis(key)
            remove_bikers_from_channel_group((request.user.userprofile.id,))

        return Response({'message':'successfully logout!'},status=status.HTTP_200_OK)
