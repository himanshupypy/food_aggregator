# from django.contrib.auth.models import (
#     AbstractBaseUser, PermissionsMixin
# )
# from django.db import models
#
#
# class User(AbstractBaseUser, PermissionsMixin):
#     USER_TYPE = (
#         ('M', 'Male'),
#         ('F', 'Female'),
#     )
#     email = models.CharField(max_length=100)
#     is_active = models.BooleanField(default=1)
#     mobile = models.CharField(max_length=10, unique=True, db_index=True)
#     first_name = models.CharField(max_length=100)
#     middle_name = models.CharField(max_length=30, null=True, blank=True)
#     last_name = models.CharField(max_length=30, blank=True)
#     user_type = models.CharField(max_length=1, choices=USER_TYPE)
#
#     def __str__(self):
#         return str(self.email)
#
