import pika
from django.conf import settings


class QueueWrapper:
    def __init__(self, username=settings.RABBITMQ_USERNAME,
                 password=settings.RABBITMQ_PASSWORD,
                 host=settings.RABBITMQ_HOST, port=settings.RABBITMQ_PORT,
                 url=settings.RABBITMQ_CONNECTION_URL
                 ):
        self.connection = None
        self.channel = None
        if not url:
            self.url_connection=pika.URLParameters(f'amqp://{username}:{password}@{host}:{port}/%2F')
        else:
            self.url_connection = pika.URLParameters(url)
        self.create_blocking_connection()
        self.create_channel_object()

    def create_blocking_connection(self):
        self.connection = pika.BlockingConnection(self.url_connection)

    def create_channel_object(self):
        self.channel = self.connection.channel()

    def publish_basic(self, queue_name, body, priority):
        self.channel.basic_publish(exchange='', routing_key=queue_name, body=body,
                              properties=pika.BasicProperties(delivery_mode=2, priority=priority))

    def consume_task(self, queue_name='priority_queue'):
        method_frame, header_frame, body = self.channel.basic_get(queue_name)
        if method_frame:
            data = {
                'method_frame': method_frame,
                'header_frame': header_frame,
                'body': body.decode('utf-8')
            }
            self.channel.basic_ack(method_frame.delivery_tag)
            return data
        else:
            return None

    def close_connection(self):
        self.channel.close()
