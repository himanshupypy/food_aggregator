from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter
import triggers.routing

ASGI_APPLICATION = "food_project.routing.application"

application = ProtocolTypeRouter({
    # (http->django views is added by default)
    'websocket': AuthMiddlewareStack(
        URLRouter(
            triggers.routing.websocket_urlpatterns
        )
    ),
})