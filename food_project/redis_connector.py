import redis


class RedisWrapper:

    def __init__(self):
        self.redis_connection = redis.StrictRedis()

    def get_redis_connection(self):
        return self.redis_connection
