import logging


def set_logger():
    logger = logging.getLogger('food-aggregator')
    logging.basicConfig(
        level=logging.INFO,
        format='%(name)s %(levelname)s %(message)s',
    )
    return logger


get_logger = set_logger()

