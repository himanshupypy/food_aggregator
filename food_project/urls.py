"""food_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
# mysite/urls.py
from django.conf.urls import include, url
from django.contrib import admin
from .views import *

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^biker-dashboard$', biker_dashboard, name='biker-dashboard'),
    url(r'^manager-dashboard$', manager_dashboard, name='biker-dashboard'),
    url(r'^login-page$', login, name='login'),
    # url(r'^biker/', include('triggers.urls')),
    url(r'^login/', view=LoginView.as_view(), name='login'),
    url(r'^logout/', view=LogoutView.as_view(), name='login'),
    url(r'^admin/', admin.site.urls),
    url(r'^task/', include('delivery_tasks.urls')),
    url(r'^create-task-view/', create_task, name='create-task'),
]