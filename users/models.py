from django.db import models
from django.contrib.auth.models import User


class UserProfile(models.Model):
    user = models.OneToOneField(User,  on_delete=models.CASCADE)
    USER_TYPE = (
        ('M', 'Manager'),
        ('D', 'Delivery Person'),
    )
    user_type = models.CharField(max_length=1, choices=USER_TYPE)


class UserChannelMapping(models.Model):
    user = models.OneToOneField(UserProfile, on_delete=models.CASCADE)
    channel_name = models.CharField(max_length=255)