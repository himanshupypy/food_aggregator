from django.conf import settings
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync

from .models import Tasks, TaskTransition
from users.models import UserChannelMapping
from food_project.redis_connector import RedisWrapper
from food_project.queue_connector import QueueWrapper

FREE_DELIVERY_PERSON_PREFIX = settings.FREE_DELIVERY_PERSON_PREFIX
CURRENT_TASK_KEY = settings.CURRENT_TASK_KEY
TASK_PRIORITY_QUEUE_NAME = settings.TASK_PRIORITY_QUEUE_NAME
REPLACE_TASK_PRIORITY_QUEUE_NAME = settings.REPLACE_TASK_PRIORITY_QUEUE_NAME
TASK_ACCEPTANCE_CHANNEL_GROUP_NAME = settings.TASK_ACCEPTANCE_CHANNEL_GROUP_NAME


def get_task_detail(task_id):
    task = Tasks.objects.filter(id=task_id).first()
    if not task:
        return {}
    data = {
        'id': task.id,
        'title': task.title,
        'priority': int(task.priority)
    }
    return data


def get_task_list_by_manager_id(manager_id):
    return_data = []
    task_list = Tasks.objects.filter(created_by_id=manager_id).all()
    for i in task_list:
        data = {
            'id': i.id,
            'title': i.title,
            'last_state': i.last_known_state,
            'priority': i.priority,
            'assigned_delivery_person': i.assigned_delivery_person.user.username
            if i.assigned_delivery_person else None,
            'last_updated': i.updated_at,
            'created_at': i.created_at
        }
        return_data.append(data)

    return return_data


def get_task_list_by_biker_id(biker_id, status='new'):
    return_data = []
    task_list = Tasks.objects.filter(
        assigned_delivery_person_id=biker_id,
        last_known_state=status
    ).all()
    for i in task_list:
        data = {
            'id': i.id,
            'title': i.title,
            'last_state': i.last_known_state,
            'priority': i.priority,
            'assigned_delivery_person': i.assigned_delivery_person.user.username
            if i.assigned_delivery_person else None,
            'last_updated': i.updated_at,
            'created_at': i.created_at
        }
        return_data.append(data)

    return return_data


def get_task_transition_by_id(task_id):
    return_data = []
    task_list = TaskTransition.objects.filter(
        task_id=task_id
    ).all()
    for i in task_list:
        data = {
            'state': i.state,
            'updated_at': i.updated_at
        }
        return_data.append(data)

    return return_data


def add_key_in_redis(key, value):
    redis_obj = RedisWrapper().get_redis_connection()
    key_added = redis_obj.set(key, str(value))
    print('key added ',key, key_added)


def delete_key_in_redis(key):
    redis_obj = RedisWrapper().get_redis_connection()
    gg = redis_obj.delete(key)
    print("ggsdgsgdss ",gg)


def get_value_from_redis(key):
    redis_obj = RedisWrapper().get_redis_connection()
    value = redis_obj.get(key)
    if not value:
        return None
    else:
        try:
            value = value.decode('utf-8')
            return value
        except Exception:
            return value


def get_free_delivery_persons():
    redis_obj = RedisWrapper().get_redis_connection()
    return [biker.decode('utf-8').split('_')[-1] for biker
            in redis_obj.scan_iter(match=f'{FREE_DELIVERY_PERSON_PREFIX}*')
            ]


def get_bikers_channel_who_rejected_order(key):
    value = get_value_from_redis(key)
    if not value:
        return None
    else:
        ids = [int(x) for x in value.split('_')]
    exclude = []
    for i in ids:
        channel_name = get_socket_channel_name_of_user(int(i))
        if not channel_name:
            print('channel not found')
            #LOG.error('channel not found for user {0}'.format(i))
        else:
            exclude.append(channel_name)
    return exclude


def get_current_priority_task_for_acceptance():
    value = get_value_from_redis(CURRENT_TASK_KEY)
    if not value:
        return None
    else:
        try:
            value = value.decode('utf-8')
        except Exception as ex:
            print('error in getting current task from redis ', ex.__str__())
            pass
        data = (int(value.split('_')[0]), int(value.split('_')[1]))
        return data


def set_current_priority_task_for_acceptance(value):
    print("inside set current function ",value, CURRENT_TASK_KEY)
    add_key_in_redis(CURRENT_TASK_KEY, value)


def send_task_in_queue(task_id, priority):
    queue_obj = QueueWrapper()
    queue_obj.publish_basic(TASK_PRIORITY_QUEUE_NAME, task_id.__str__(), priority)
    queue_obj.close_connection()


def send_task_in_replace_queue(task_id, priority):
    queue_obj = QueueWrapper()
    queue_obj.publish_basic(REPLACE_TASK_PRIORITY_QUEUE_NAME, task_id.__str__(), priority)
    queue_obj.close_connection()


def broadcast_task_to_bikers_for_acceptance(task_data, task_type='new', exclude=None):
    task_data['channel_message_type'] = task_type
    channel_layer = get_channel_layer()
    if exclude:
        for channel in exclude:
            async_to_sync(channel_layer.group_discard)(
                TASK_ACCEPTANCE_CHANNEL_GROUP_NAME,
                channel
            )
    async_to_sync(channel_layer.group_send)(
        TASK_ACCEPTANCE_CHANNEL_GROUP_NAME,
        {
            'type': 'group_message',
            'message': task_data
        }
    )
    if exclude:
        for channel in exclude:
            async_to_sync(channel_layer.group_add)(
                TASK_ACCEPTANCE_CHANNEL_GROUP_NAME,
                channel

            )


def broadcast_task_to_manager_for_declined(
        task_data, manager_id, task_type='new', exclude=None
):
    task_data['channel_message_type'] = task_type
    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.group_send)(
        f'manager_group_{manager_id}',
        {
            'type': 'group_message',
            'message': task_data
        }
    )


def consume_task_from_queue(queue_name=TASK_PRIORITY_QUEUE_NAME):
    queue_obj = QueueWrapper()
    task_data = queue_obj.consume_task(queue_name=queue_name)
    queue_obj.close_connection()
    return task_data


def get_socket_channel_name_of_user(user_id):
    channel_data = UserChannelMapping.objects.filter(user_id=user_id).first()
    if not channel_data:
        return None
    else:
        return channel_data.channel_name


def remove_bikers_from_channel_group(biker_ids):
    channel_layer = get_channel_layer()
    for biker in biker_ids:
        channel = get_socket_channel_name_of_user(user_id=int(biker))
        if not channel:
            continue
        async_to_sync(channel_layer.group_discard)(
            TASK_ACCEPTANCE_CHANNEL_GROUP_NAME,
            channel
        )


def add_bikers_into_channel_group(biker_ids):
    channel_layer = get_channel_layer()
    for biker in biker_ids:
        channel = get_socket_channel_name_of_user(user_id=int(biker))
        if not channel:
            continue
        async_to_sync(channel_layer.group_add)(
            TASK_ACCEPTANCE_CHANNEL_GROUP_NAME,
            channel
        )


def add_biker_in_rejected_task_in_redis(task_id, biker_profile_id):
    key = task_id.__str__()
    value = get_value_from_redis(key)
    if not value:
        inserted_value = biker_profile_id.__str__()
    else:
        inserted_value = f'{value}_{biker_profile_id}'

    add_key_in_redis(key, inserted_value)