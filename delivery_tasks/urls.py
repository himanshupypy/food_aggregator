from django.urls import path
from .views import (
    GetManagerTaskList, GetBikerTaskList, CreateTask, GetTaskTransition, UpdateTaskView
)


urlpatterns = [
    path("tasks/", GetManagerTaskList.as_view(), name="get-task-list"),
    path("biker-tasks/", GetBikerTaskList.as_view(), name="get-biker-task-list"),
    path("create-task/", CreateTask.as_view(), name='create-task-api'),
    path("task-transition/<int:task_id>/", GetTaskTransition.as_view(), name="get-transition"),
    path("update-task/<int:task_id>/", UpdateTaskView.as_view(), name="update-task"),
]