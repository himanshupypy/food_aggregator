from django.conf import settings
from .helper import (
    get_free_delivery_persons, get_task_detail, get_current_priority_task_for_acceptance,
    broadcast_task_to_bikers_for_acceptance, consume_task_from_queue,
    set_current_priority_task_for_acceptance, send_task_in_replace_queue, get_bikers_channel_who_rejected_order,
    delete_key_in_redis, add_key_in_redis, remove_bikers_from_channel_group, add_bikers_into_channel_group,
    add_biker_in_rejected_task_in_redis, send_task_in_queue, broadcast_task_to_manager_for_declined, get_value_from_redis
)
from .models import Tasks, TaskTransition
from food_project.logger import get_logger as LOG


FREE_DELIVERY_PERSON_PREFIX = settings.FREE_DELIVERY_PERSON_PREFIX
CURRENT_TASK_KEY = settings.CURRENT_TASK_KEY
TASK_PRIORITY_QUEUE_NAME = settings.TASK_PRIORITY_QUEUE_NAME
REPLACE_TASK_PRIORITY_QUEUE_NAME = settings.REPLACE_TASK_PRIORITY_QUEUE_NAME
TASK_ACCEPTANCE_CHANNEL_GROUP_NAME = settings.TASK_ACCEPTANCE_CHANNEL_GROUP_NAME
CANCELLED_TASK_KEY = settings.CANCELLED_TASK_KEY


def send_task_for_acceptance_to_biker_on_new_task(task_id):
    # import pdb
    # pdb.set_trace()
    task_detail = get_task_detail(task_id)
    task_for_acceptance = get_current_priority_task_for_acceptance()
    print('task for acceptance ',task_for_acceptance)
    if not task_detail:
        LOG.error('there is not task related to this id {0}'.format(task_id))
        return
    # bikers = get_free_delivery_persons()
    # if not bikers:
    #     if not task_for_acceptance:
    #         set_current_priority_task_for_acceptance(f'{task_detail["id"]}_{task_detail["priority"]}')
    #         LOG.info('no biker available to take the order')
    #     else:
    #         pass


    # if no current task for acceptance we can through the task new task to the
    # available delivery persons
    if not task_for_acceptance:
        # # consume task from priority queue and broadcast it
        # task_data = consume_task_from_queue(queue_name=TASK_PRIORITY_QUEUE_NAME)
        # if task_data:
        queue_task_id = int(task_detail['id'])
        bikers_who_rejected = get_bikers_channel_who_rejected_order(queue_task_id.__str__())
        queue_task_detail = get_task_detail(queue_task_id)
        set_current_priority_task_for_acceptance(f'{queue_task_id}_{queue_task_detail["priority"]}')
        broadcast_task_to_bikers_for_acceptance(queue_task_detail, task_type='new', exclude=bikers_who_rejected)
        return

    # if already have the task, we need to check the priority accordingly we will broadcast the task
    old_task_id = task_for_acceptance[0]
    old_task_priority = task_for_acceptance[1]
    if old_task_priority < task_detail['priority']:
        send_task_in_replace_queue(old_task_id, old_task_priority)
        bikers_who_rejected = get_bikers_channel_who_rejected_order(task_detail["id"].__str__())
        set_current_priority_task_for_acceptance(f'{task_detail["id"]}_{task_detail["priority"]}')
        broadcast_task_to_bikers_for_acceptance(task_detail, task_type='replace', exclude=bikers_who_rejected)
    else:
        send_task_in_queue(task_detail['id'], int(task_detail['priority']))


def send_task_for_acceptance_to_biker_on_accepted():
    found_task = False
    cancel_task = get_value_from_redis(CANCELLED_TASK_KEY)
    cancel_task_list = []
    if cancel_task:
        try:
            cancel_task = cancel_task.decode('utf-8')
        except Exception:
            pass
        cancel_task_list = cancel_task.split('_')
    while not found_task:
        task_for_acceptance = get_current_priority_task_for_acceptance()
        if task_for_acceptance:
            found_task = True
            continue
        task_data = consume_task_from_queue(queue_name=REPLACE_TASK_PRIORITY_QUEUE_NAME)
        if not task_data:
            # consume task from priority queue and broadcast it
            task_data = consume_task_from_queue(queue_name=TASK_PRIORITY_QUEUE_NAME)
        if not task_data:
            LOG.info('queue is empty ')
            found_task = True
            continue
        queue_task_id = task_data['body']
        if cancel_task_list:
            if str(queue_task_id) in cancel_task_list:
                print('task was cancelled by the manager')
                cancel_task_list.remove(str(queue_task_id))
                cancel_task_value = '_'.join(cancel_task_list)
                add_key_in_redis(CANCELLED_TASK_KEY, cancel_task_value)
                continue
        queue_task_detail = get_task_detail(queue_task_id)
        value = f'{queue_task_detail["id"]}_{queue_task_detail["priority"]}'
        bikers_who_rejected = get_bikers_channel_who_rejected_order(queue_task_detail["id"].__str__())
        set_current_priority_task_for_acceptance(value)
        broadcast_task_to_bikers_for_acceptance(
            queue_task_detail, task_type='new', exclude=bikers_who_rejected
        )
        found_task = True


def send_task_for_acceptance_to_biker_on_completed():
    send_task_for_acceptance_to_biker_on_accepted()


def send_task_for_acceptance_to_biker_on_declined(task_id):
    send_task_for_acceptance_to_biker_on_new_task(task_id)


def send_task_for_acceptance_to_biker_on_cancelled(task_id):
    value = get_value_from_redis(CANCELLED_TASK_KEY)
    if not value:
        value = task_id
        add_key_in_redis(CANCELLED_TASK_KEY, value)
    else:
        task_ids = value.split('_')
        if str(task_id) not in task_id:
            task_ids.append(str(task_id))
            value = '_'.join(task_ids)
            add_key_in_redis(CANCELLED_TASK_KEY, value)
    task_for_acceptance = get_current_priority_task_for_acceptance()
    if task_for_acceptance:
        if task_for_acceptance[0] == task_id:
            delete_key_in_redis(CURRENT_TASK_KEY)
    send_task_for_acceptance_to_biker_on_accepted()


def accepted_task_list_of_biker(biker_profile_id):
    task_count = Tasks.objects.filter(
        assigned_delivery_person_id=biker_profile_id, last_known_state='accepted'
    ).count()

    return task_count


def update_task_data_on_order_accepted(task_id, userprofile):
    task = Tasks.objects.filter(id=task_id).first()
    if not task:
        return {
            'status': 0,
            'message': 'task not exists.'
        }
    if task.last_known_state != 'new':
        if not task:
            return {
                'status': 0,
                'message': 'task not in new state.'
            }
    task.last_known_state = 'accepted'
    task.assigned_delivery_person = userprofile
    task.save()
    task_transition = TaskTransition(
        task=task,
        state='accepted'
    )
    task_transition.save()

    return {
        'status': 1
    }


def update_task_data_on_order_completed(task_id):
    task = Tasks.objects.filter(id=task_id).first()
    if not task:
        return {
            'status': 0,
            'message': 'task not exists.'
        }
    if task.last_known_state != 'accepted':
        if not task:
            return {
                'status': 0,
                'message': 'task not in accepted state.'
            }
    task.last_known_state = 'completed'
    task.save()
    task_transition = TaskTransition(
        task=task,
        state='completed'
    )
    task_transition.save()

    return {
        'status': 1
    }


def update_queue_data_on_order_accepted(biker_profile):
    print('current task key ',CURRENT_TASK_KEY)
    delete_key_in_redis(CURRENT_TASK_KEY)
    count = accepted_task_list_of_biker(biker_profile.id)
    if count >= 3:
        # delete biker from free biker queue
        biker_key = f'{FREE_DELIVERY_PERSON_PREFIX}{biker_profile.id}'
        delete_key_in_redis(biker_key)
        # delete biker from channel group
        remove_bikers_from_channel_group((biker_profile.id,))


def update_queue_data_on_order_completed(biker_profile):
    count = accepted_task_list_of_biker(biker_profile.id)
    if count <= 2:
        # add biker from free biker queue
        biker_key = f'{FREE_DELIVERY_PERSON_PREFIX}{biker_profile.id}'
        add_key_in_redis(biker_key, 1)
        # delete biker from channel group
        add_bikers_into_channel_group((biker_profile.id,))


def update_task_data_on_order_declined(task_id):
    task = Tasks.objects.filter(id=task_id).first()
    if not task:
        return {
            'status': 0,
            'message': 'task not exists.'
        }
    if task.last_known_state != 'accepted':
        if not task:
            return {
                'status': 0,
                'message': 'task not in accepted state.'
            }
    task.last_known_state = 'new'
    task.assigned_delivery_person = None
    task.save()
    task_transition = TaskTransition(
        task=task,
        state='declined'
    )
    task_transition.save()

    return {
        'status': 1
    }


def update_queue_data_on_order_declined(task_id, biker_profile):
    count = accepted_task_list_of_biker(biker_profile.id)
    add_biker_in_rejected_task_in_redis(task_id, biker_profile.id)
    if count <= 2:
        # add biker from free biker queue
        biker_key = f'{FREE_DELIVERY_PERSON_PREFIX}{biker_profile.id}'
        add_key_in_redis(biker_key, 1)
        # delete biker from channel group
        add_bikers_into_channel_group((biker_profile.id,))


def update_queue_data_on_order_cancelled(task_id, biker_profile):
    delete_key_in_redis(CURRENT_TASK_KEY)
    count = accepted_task_list_of_biker(biker_profile.id)
    add_biker_in_rejected_task_in_redis(task_id, biker_profile.id)
    if count <= 2:
        # add biker in free biker queue
        biker_key = f'{FREE_DELIVERY_PERSON_PREFIX}{biker_profile.id}'
        add_key_in_redis(biker_key, 1)
        # add biker in channel group
        add_bikers_into_channel_group((biker_profile.id,))


def update_task_data_on_order_cancelled(task_id):
    task = Tasks.objects.filter(id=task_id).first()
    if not task:
        return {
            'status': 0,
            'message': 'task not exists.'
        }
    if task.last_known_state != 'new':
        if not task:
            return {
                'status': 0,
                'message': 'task not in new state.'
            }
    task.last_known_state = 'cancelled'
    task.save()
    task_transition = TaskTransition(
        task=task,
        state='cancelled'
    )
    task_transition.save()

    return {
        'status': 1
    }


def send_trigger_to_biker_for_remove_task(task_id):
    bikers_who_rejected = get_bikers_channel_who_rejected_order(task_id.__str__())
    task_detail = {
        'id': task_id
    }
    broadcast_task_to_bikers_for_acceptance(task_detail, task_type='remove', exclude=bikers_who_rejected)


def send_trigger_to_manager_for_task_updation(task_id, manager_id, action='accepted'):
    task_detail = {
        'id': task_id,
    }
    broadcast_task_to_manager_for_declined(task_detail, manager_id, task_type=action)
