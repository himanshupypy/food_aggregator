from django.db import models
from users.models import UserProfile
import datetime
# Create your models here.


class Tasks(models.Model):
    PRIORITY_TYPE = (
        ('3', 'High'),
        ('2', 'Medium'),
        ('1', 'Low'),
    )
    title = models.CharField(max_length=255)
    priority = models.CharField(max_length=1, choices=PRIORITY_TYPE)
    created_by = models.ForeignKey(UserProfile, on_delete=models.CASCADE)
    assigned_delivery_person = models.ForeignKey(
        UserProfile, on_delete=models.CASCADE,
        related_name='assigned_person', null=True
    )
    last_known_state = models.CharField(max_length=30, default='new')
    created_at = models.DateTimeField(default=datetime.datetime.now)
    updated_at = models.DateTimeField(auto_now=True)


class TaskTransition(models.Model):
    task = models.ForeignKey(Tasks, on_delete=models.CASCADE)
    state = models.CharField(max_length=30)
    updated_at = models.DateTimeField(auto_now=True)
