from rest_framework import generics, status, permissions
from rest_framework.response import Response

from .helper import get_task_list_by_manager_id, get_task_list_by_biker_id
from .delivery_service import (
    update_queue_data_on_order_accepted, update_task_data_on_order_accepted, update_task_data_on_order_completed,
    send_task_for_acceptance_to_biker_on_accepted, update_queue_data_on_order_completed,
    send_task_for_acceptance_to_biker_on_completed, update_task_data_on_order_declined,
    send_task_for_acceptance_to_biker_on_declined, update_queue_data_on_order_declined,
    update_task_data_on_order_cancelled, update_queue_data_on_order_cancelled,
    send_task_for_acceptance_to_biker_on_cancelled, send_trigger_to_biker_for_remove_task,
    send_task_for_acceptance_to_biker_on_new_task, send_trigger_to_manager_for_task_updation
)
from .models import Tasks, TaskTransition
# Create your views here.


class GetManagerTaskList(generics.GenericAPIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, *args, **kwrags):
        user = request.user
        user_profile_id = user.userprofile.id
        data = get_task_list_by_manager_id(user_profile_id)
        print('data ',data)
        return Response(
            {
                "data": {
                   'task_list': data
                }
            },
            status=status.HTTP_200_OK
        )


class GetBikerTaskList(generics.GenericAPIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, *args, **kwrags):
        user = request.user
        user_profile_id = user.userprofile.id
        data = get_task_list_by_biker_id(user_profile_id, status='accepted')
        return Response(
            {
                "data": {
                   'task_list': data
                }
            },
            status=status.HTTP_200_OK
        )


class GetTaskTransition(generics.GenericAPIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, task_id, *args, **kwrags):
        user = request.user
        user_profile_id = user.userprofile.id
        data = get_task_list_by_biker_id(user_profile_id, status='accepted')
        return Response(
            {
                "data": {
                   'task_list': data
                }
            },
            status=status.HTTP_200_OK
        )


class CreateTask(generics.GenericAPIView):
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        title = request.data.get("title", "")
        priority = request.data.get("priority", "")
        task = Tasks(
            title=title,
            priority=priority,
            created_by_id=request.user.userprofile.id
        )
        task.save()
        task.refresh_from_db()
        transition = TaskTransition(
            task=task,
            state='new'
        )
        transition.save()
        send_task_for_acceptance_to_biker_on_new_task(task.id)
        return Response(
            {
                'message': 'successfully created'
            },
            status=status.HTTP_200_OK
        )


class UpdateTaskView(generics.GenericAPIView):
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, task_id, *args, **kwrags):
        user_action = request.data.get("action", "")
        task_id = int(task_id)
        if user_action not in ['completed', 'accepted', 'declined', 'cancelled']:
            return Response(
                {
                    'message': 'user action not supported'
                },
                status=status.HTTP_400_BAD_REQUEST
            )
        userprofile = request.user.userprofile
        if user_action == 'accepted':
            updated_data = update_task_data_on_order_accepted(task_id, userprofile)

        elif user_action == 'completed':
            updated_data = update_task_data_on_order_completed(task_id)

        elif user_action == 'declined':
            updated_data = update_task_data_on_order_declined(task_id)

        else:
            updated_data = update_task_data_on_order_cancelled(task_id)
        if not updated_data['status']:
            return Response(
                {
                    'message': updated_data['message']
                },
                status=status.HTTP_400_BAD_REQUEST
            )
        task_data = Tasks.objects.filter(id=int(task_id)).first()
        if user_action == 'accepted':
            send_trigger_to_biker_for_remove_task(task_id)
            send_trigger_to_manager_for_task_updation(
                task_id, task_data.created_by_id, action='accepted'
            )
            update_queue_data_on_order_accepted(userprofile)
            send_task_for_acceptance_to_biker_on_accepted()

        elif user_action == 'completed':
            update_queue_data_on_order_completed(userprofile)
            send_trigger_to_manager_for_task_updation(
                task_id, task_data.created_by_id, action='completed'
            )
            send_task_for_acceptance_to_biker_on_completed()

        elif user_action == 'declined':
            send_trigger_to_manager_for_task_updation(
                task_id, task_data.created_by_id, action='declined'
            )
            update_queue_data_on_order_declined(task_id, userprofile)
            send_task_for_acceptance_to_biker_on_declined(task_id)

        else:
            update_queue_data_on_order_cancelled(task_id, userprofile)
            send_task_for_acceptance_to_biker_on_cancelled(task_id)

        return Response(
            {
                'message': 'successfully updated'
            },
            status=status.HTTP_200_OK
        )