from django.apps import AppConfig


class DeliveryTasksConfig(AppConfig):
    name = 'delivery_tasks'
