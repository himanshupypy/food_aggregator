from django.conf import settings
from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
from users.models import UserChannelMapping
from delivery_tasks.helper import delete_key_in_redis, remove_bikers_from_channel_group,\
    get_current_priority_task_for_acceptance, get_bikers_channel_who_rejected_order, \
    broadcast_task_to_bikers_for_acceptance, get_task_detail, add_key_in_redis
from delivery_tasks.delivery_service import accepted_task_list_of_biker
import json

FREE_DELIVERY_PERSON_PREFIX = settings.FREE_DELIVERY_PERSON_PREFIX
CURRENT_TASK_KEY = settings.CURRENT_TASK_KEY
TASK_PRIORITY_QUEUE_NAME = settings.TASK_PRIORITY_QUEUE_NAME
REPLACE_TASK_PRIORITY_QUEUE_NAME = settings.REPLACE_TASK_PRIORITY_QUEUE_NAME
TASK_ACCEPTANCE_CHANNEL_GROUP_NAME = settings.TASK_ACCEPTANCE_CHANNEL_GROUP_NAME


class ManagerConsumer(WebsocketConsumer):
    def connect(self):
        print('channel name ',self.channel_name)
        self.user_id = self.scope['url_route']['kwargs']['user_id']
        self.room_group_name = f'manager_group_{self.user_id}'
        channel = UserChannelMapping.objects.filter(user_id=int(self.user_id)).first()
        if not channel:
            channel = UserChannelMapping(
                user_id=int(self.user_id), channel_name=self.channel_name
            )
            channel.save()
        else:
            channel.channel_name = self.channel_name
            channel.save()
        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name
        )
        self.accept()

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']

        # Send message to room group
        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            {
                'type': 'group_message',
                'message': message
            }
        )

    # Receive message from room group
    def group_message(self, event):
        message = event['message']

        # Send message to WebSocket
        self.send(text_data=json.dumps({
            'message': message
        }))


class BikerConsumer(WebsocketConsumer):
    def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['user_id']
        self.room_group_name = TASK_ACCEPTANCE_CHANNEL_GROUP_NAME
        print(self.channel_name)
        # Join room group
        channel = UserChannelMapping.objects.filter(user_id=int(self.room_name)).first()
        if not channel:
            channel = UserChannelMapping(user_id=int(self.room_name), channel_name=self.channel_name)
            channel.save()
        else:
            channel.channel_name = self.channel_name
            channel.save()
        count = accepted_task_list_of_biker(int(self.room_name))
        print('count is ',count)
        if count < 3:
            key = f'{FREE_DELIVERY_PERSON_PREFIX}{self.room_name}'
            add_key_in_redis(key, 1)
            async_to_sync(self.channel_layer.group_add)(
                self.room_group_name,
                self.channel_name
            )

        self.accept()
        task_for_acceptance = get_current_priority_task_for_acceptance()
        print('task for acceptance ',task_for_acceptance)
        if count < 3 and task_for_acceptance:
            task_id = task_for_acceptance[0]
            task_detail = get_task_detail(task_id)
            if task_detail:
                bikers_who_rejected = get_bikers_channel_who_rejected_order(
                    task_detail["id"].__str__()
                )
                broadcast_task_to_bikers_for_acceptance(
                    task_detail, task_type='new', exclude=bikers_who_rejected
                )

    def disconnect(self, close_code):
        # Leave room group
        print('leaving group ',self.room_name)
        key = f'{FREE_DELIVERY_PERSON_PREFIX}{self.room_name}'
        delete_key_in_redis(key)
        remove_bikers_from_channel_group((int(self.room_name),))
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']

        # Send message to room group
        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            {
                'type': 'group_message',
                'message': message
            }
        )

    # Receive message from room group
    def group_message(self, event):
        message = event['message']

        # Send message to WebSocket
        self.send(text_data=json.dumps({
            'message': message
        }))