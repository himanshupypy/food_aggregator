# chat/routing.py
from django.conf.urls import url

from . import consumers

websocket_urlpatterns = [
    url(r'^ws/biker/(?P<user_id>[^/]+)/$', consumers.BikerConsumer),
    url(r'^ws/manager/(?P<user_id>[^/]+)/$', consumers.ManagerConsumer),
]